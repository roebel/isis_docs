The ISiS software is a command line application for singing synthesis that can be used to generate
singing signals by means of synthesizing them from melody and lyrics.


The ISiS software is the result of the French national project  [ChaNTeR](https://www.ircam.fr/projects/pages/chanter)
([Project ANR-13-CORD-011](https://anr.fr/Projet-ANR-13-CORD-0011)),
that was performed in collaboration with [Acapela](https://www.acapela-group.com/fr/), [LIMSI](https://www.limsi.fr/en/), and [Dualo](https://dualo.org/fr/).

![](https://forum.ircam.fr/media/uploads/software/ISiS/ms_dsc_0222-750x500.jpg) ![](https://forum.ircam.fr/media/uploads/software/ISiS/rt_dsc_0328-750x500.jpg)

ISiS is distributed free of charge for members of the IRCAM Forum from the download links above.
You will need to download the application for your system (Linux or MacOS the file .dmg) and the 
singing databases you would like to use.

ISiS operates offline reading score and lyrics from file and renders the results into an output
sound file. In its current version ISiS supports synthesis with 3 French singing voices:

    RT: a tenor male pop singer, and
    MS: a female mezzo-soprano pop singer, and
    EL: a female soprano lyrical singer.

The current database files are version 1.1.0 and available in the release 1.2.7 
[here](https://forum.ircam.fr/projects/releases/isis/).

Basic documentation is available [here](https://isis-documentation.readthedocs.io/en/latest/).

## ChangeLog

### ISiS v1.3.0 (2024-11-03)

- New features:

  - Changed internal representation to use a dedicated song class
  - Removed support for storing intermediate synthesis states into XML files and replaced them by a json file containing the score, lyrics (phonemes), F0 and loudness information.
  - Introduce the notion of three synthesis stages [SCORE, INTERPRETATION, AUDIO]. The SCORE stage is finished after reading the configuration files and generating the internal song representation. The INTERPRETATION stage is finished once all style information has been applied and the internal song is ready for synthesis. The AUDIO stage is the final stage that ends with the synthesis of the output audio file. The SCORE and INTERPRETATION stages are intermediate stages, and do not produce an audio output. These stages can be used to generate the json file representing the state of the SONG class at the given stage. Note that the json files can then be read and modified by means of external scripts, and then reread by the ISiS software to proceed with the synthesis until the final AUDIO stage. 

  - Added new script text2score.sh that allows creating an ISiS score.cfg file from a given text, the score file will have the correct number of notes all with the same pitch and duration and is meant to be used as starting point for further editing. It contains the correct number of notes matching the phonemization of the text.

- Updates:
  - Added support for natively running on MacOS ARM64 processors.
  - Updated implementation using python 3.9, numpy 1.26.4, and pyinstaller 6.5.0.
  - Established mechanisms to update singing style models to more recent versions of scikit-learn and updated style models to decision trees format from scikit-learn 1.0.2.

- Bug fixes:
  - Fixed bug in loudness accents that may create plops at the transition from a syllable to a rest. https://discussion.forum.ircam.fr/t/questions-and-observations-silences-accents-problem-phonemes/90550/4?u=roebel

- **Singer databases**

  - Singer databases have been updated for version 1.3.0. They have received numerous corrections related to wrong label annotations/alignments, some of which did lead to crashes of the ISiS software.


### ISiS v1.2.9

- bug fix release for IRCAM Forum 2021.
- improved logging.


### ISiS v1.2.8

- added command line option --env_transposition_fac, configSynth transposition_env_exp: more natural singing quality by means of dynamic envelop transposition,
- faster synthesis (about 10-30%) byt means of improved FFT library and pulse parameter cache,
- signficiantly reduced memory requirement: now allows synthesizing very long sogs with constant memory,
- support storing of, and resynthesis from semi-symbolic XML files for F0 and loudness contours,
- added progress indicator to the pulse generation phase that may last rather long (30-80% of song duration).


### ISiS Version 1.2.7

  - slightly imporved efficiency in pulse generator
  - fixed undesired pulses at borders of unvoiced segments 
  - Sligtly improved install script to be more robust against
  calling it without full path, and in case of spaces in directory names.
  
### ISiS Version 1.2.6

  - many bug fixes, first release for Linux.

### ISiS Version 1.2.5

 does no longer require to use shell environment variables for voice configuration, which  simplifies
 running the ISiS software  under Max/MSP.
 

 > - [Sound Analysis-Synthesis](https://www.ircam.fr/recherche/equipes-recherche/anasyn/) Team
