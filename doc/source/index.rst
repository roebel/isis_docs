.. ISiS_doc documentation master file, created by
   sphinx-quickstart on Tue Jun 12 15:03:55 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ISiS's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   ISiS Introduction <Intro>


Indices and tables
==================

* :ref:`search`
