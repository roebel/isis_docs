.. _isis-score-files:

ISiS scores
-----------

ISiS score files are simple text files with configfile syntax as
described in the documentation of python's `config parser
module <https://docs.python.org/3.6/library/configparser.html#supported-ini-file-structure>`__.

Configuration file syntax
~~~~~~~~~~~~~~~~~~~~~~~~~

A configuration file consists of *sections*, each led by a section
header marked by means of square brackets, followed by key/value entries
separated by a specific string (either = or : may be used). By default,
section names are case sensitive but keys are not. Leading and trailing
whitespaces are removed from keys and values. Values can also span
multiple lines, as long as they are indented deeper than the first line
of the value. Empty lines that contain white spaces will be treated as
continuation lines of the same sequence of values. Configuration files
may include comments, prefixed by specific characters (# or ;). Comments
may appear on their own on an otherwise empty line, possibly indented.

With respect to section and key names it is important to understand that
when reading a config file, the parser will only consider known headers
and keys, all unknown headers and keys will be silently ignored.

Score example
~~~~~~~~~~~~~

There are two obligatory sections that have to be present in a score:
the `[lyrics]` section specifying the lyrics to be sung in terms
`XSAMPA <https://en.wikipedia.org/wiki/X-SAMPA>`__ phonemes, and the
`[score]` section, that contains the melody description.

We will describe the keys that make up the two sections by means of
looking into the default score file that is delivered with ISiS and will
be used whenever no score has been specified on the command line.

The default score
^^^^^^^^^^^^^^^^^

.. raw:: html

   <pre>
   [lyrics]
   # use one or the other, disable 
   xsampa: # _ c'est une chan-son
           _ s E t y n S a~ s o~        
           # qui nous re - sem - ble
           k i n u R @ s a~ b l @ _     
           # toi tu m'ai - mait 
           t w a t y m E m E     
           # et je t'ai - mait 
           e Z @ t E m E _              

   [score]
   midiNotes: # _ c'est une chan-son
              0,  64, 66, 67, 72, 
              # qui nous re-sem-ble
              62, 64, 66, 71, 71, 0, 
              # toi tu m'ai - mait 
              60, 62, 64, 69, 
              # et je t'ai - mait 
              59, 61, 63, 67, 0

   # transposition in midi notes
   globalTransposition: 0

   rhythm: # _ c'est une chan - son
           2, 1.54583333, 1.525, 1.525, 7.25833333, 
           # qui nous re - sem - ble
           1.68541667, 1.31666667, 1.42291667, 2.98958333, 4.575, 
           # toi tu m'ai - mait 
           1.5, 1.88333333, 1.525, 1.525, 7.33958333, 
           # et je t'ai - mait 
           1.8125, 1.52708333, 1.35416667, 6.07083333, 2

   defaultSentenceLoudness: 0.5

   tempo: 213
   </pre>

Lyrics section
~~~~~~~~~~~~~~

The lyrics section contains a single required key termed **xsampa** that
contains as value the song lyrics in form of a sequence of phonemes
expressed in the phonetic alphabet
`XSAMPA <https://fr.wikipedia.org/wiki/X-SAMPA>`__. The specification of
the lyrics in form of normal text is not supported due to the many
possible pronunciation variants that are possible for a given text.

The ISiS system requires the presence of a phoneme to be sung in the
singing database. The list of phonemes that are available in the
existing singing databases are sufficient to synthesize all French
words, they are the following

+-----------------------+---------------------------------------------------+
| Phoneme class         | phonemes                                          |
+=======================+===================================================+
| vowels                | a, e, E, 2, 9, @, i, o, O, u, y, o~, a~, e~, 9~   |
+-----------------------+---------------------------------------------------+
| semi vowels           | w, j, H                                           |
+-----------------------+---------------------------------------------------+
| voiced fricatives     | v, z, Z                                           |
+-----------------------+---------------------------------------------------+
| unvoiced fricatives   | f, s, S                                           |
+-----------------------+---------------------------------------------------+
| voiced plosives       | b, d, g                                           |
+-----------------------+---------------------------------------------------+
| unvoiced plosives     | p, t, k                                           |
+-----------------------+---------------------------------------------------+
| nasals                | m, n, N                                           |
+-----------------------+---------------------------------------------------+
| other                 | R, l                                              |
+-----------------------+---------------------------------------------------+

Find more explanations about the translation of text into XSAMPA on the
xsampa example page


.. toctree::
   :maxdepth: 2
     
   Text to XSAMPA translation <XSampa>

Score section
~~~~~~~~~~~~~

The score section needs to intends to provide the information about the
melody to be sung. There exist two means to specify the melody. The
first one consists of explicitly writing the note information into the
score section by means of specifying **midiNotes**, **tempo**, and
**rhythm**, as well as an optional **loud\_accents** key.

In the near future other score specifications methods will be added that
will allow reading the score from midi or MusicXML files.

Explicit notation
^^^^^^^^^^^^^^^^^

The explicit notation of the melody uses the following terms:

**midiNotes**: the **midiNotes** value specifies the melody to be sung
in terms of midi notes. The conversion between midi note number and
frequency in Hz is given by

Note\_hz = 440.0 \* 2.0\*\*((midiNote - 69)/ 12)

Accordingly, midi note number 69 represents the note A4 with fundamental
frequency of 440Hz, and increasing the note frequency by 1 increases the
fundamental frequency by one half tone. Note, that in an extension of
the normal midi notation ISiS note numbers are not constrained to be
integers. This means that quarter note intervals can be represented
easily by means of using 0.5 steps, that means the quarter tone above A4
would be denoted as midi note 69.5.

**tempo** : the **tempo** value defines the BPM, that is in form of the
number of quarter notes per minute. The length of a single quart note in
seconds is then

nl\_s = 60 / **tempo**

**rhythm**: the **rhythm** value determines the length of all the notes
in the **melody** section. The note length is specified in quarter
notes, which means that noting a one here will produce a note of the
length of a quarter note.

**loud\_accents**: the **loud\_accents** value can be used to control
the dynamic of individual notes. In cse no loudness accents are
specified the note loudness will be obtained from the singer database,
where all singers have sung with approximately constant loudness. The
same result will be obtained if for each note an loudness accent value
of 1 is provided. In case you would like to increase or decrease the
loudness of individual notes, you can specify arbitrary positive value
as loudness accents, that are interpreted as factors to be applied to
perceived loudness, such that specifying a loudness accent of 2 for an
individual note will render the note approximately twice as loud.

Reading the score from a midi file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Attention: this functionality is not yet finalized and still requires
implementation and testing**

.. raw:: html

   <!--
   In case you like to experiment with reading midi files, you first need to rename any existing __midiNotes__ section
   because a __midiNotes__ section always takes precedence over other input modes. For this you can rename the section to 
   an arbitrary name as for example __midiNotes_off__, so that the config file parser will silently ignore the key.

   Then you can  provide a key termed __midiFile__ which will direct the parser to read the midiFile and retrieve tempo, 
   rhythm and notes form that file.
   -->

Reading the score from a MusicXML file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Attention: this functionality is not yet finalized and requires
implementation and testing**

.. raw:: html

   <!--
   The same procedure that has been described under the previous section is valid as well for music xml files. 
   In this case, however,
   -->
