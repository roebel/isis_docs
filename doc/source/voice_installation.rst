.. _ISiS_databases:

ISiS Databases
==============

ISiS singing voices are distributed via the `ISiS
page <https://forum.ircam.fr/projects/detail/isis/>`__ of the `IRCAM
Forum <http://forumnet.ircam.fr>`__. Each voice comes as an independent
tar archive containing all sound parameters of a single voice.
Additionally, each voice arcive contains a small script that can be used
to configure a voice as ISiS default voice.

Currently there are three voices available (downloads are for Forum
members only):

1. **RT**: a tenor male pop singer:
   (`download <https://forum.ircam.fr/asset/44/RT_1.0.2.tar.bz2>`__),
   and
2. **MS**: a female mezzo-soprano pop singer
   (`download <https://forum.ircam.fr/asset/45/MS_1.0.2.tar.bz2>`__),
   and
3. **EL**: a female soprano lyrical singer
   (`download <https://forum.ircam.fr/asset/46/EL_1.0.2.tar.bz2>`__).

.. _install-voice-database:

Install a new ISiS singing voice
--------------------------------

For the ISiS program to be able to find the singing databases all the
singing voices have to be gathered in a common root directory, which we
will call ISIS_CORPORA. In the following we will assume you want to use
the directory `/Data/ISiS_DB` to collect your ISiS voices, but you are
free to choose any other place that better fits your needs. To prepare
the installation of the voices you should create the ISIS_CORPORA
directory. You can do this either from the terminal using the command
mkdir, or from your GUI (Finder on MacOS and for example KDE/Dolphin or
Gnome/Nautilus on Linux. While this directory can be located anywhere,
the voices require in the order of 1GB per singing voice, so please be
sure that the disk that you select when creating the directory has at
least 3GB of space available.

To proceed with the installation of the singing voices please download
the different voices and unpack them into the ISIS_CORPORA directory.
The voice database archives are compatible with Linux and Mac OS. You
simply save the archive into the ISIS_CORPORA directory and unpack the
tar.bz2 file you just downloaded. On `MacOS` you can simply
double-click the file in the Finder. Under Linux the operation to be
performed depends on the file manager you use, in some environments you
can double-clicking the archive to open it in a dedicated application
that you may then use to extract the content to the target directory,
other filemanagers may allow right-clicking and selecting something like
`unpack here`.

Finally, on MacOS and Linux you can also use the terminal to unpack the
voices. For brevity we will not cover this process in detail here.

The name of the folder you create will depend on the software you use
for unpacking. For ease of use it is strongly recommended to rename the
unpacked voice folder using a short ideally two character long name. In
the following we will assume you use the names `RT`, `EL` and
`MS`.

Configuring ISiS voices environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| The ISiS software uses two environment variables to find its voices.
  These two variables can be defined manually or using an automatic
  script that needs to be executed in the targeted shell environment.
| We will first describe the automatic configuration and in the
  subsequent paragraph the automatic configuration.

.. _configure-isis-corpora-root-dir:

Automatic voice configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The automatic voice configuration currently supports only `bash` (the
current default shell under `MacOS`) and `tcsh` shells. In case you
use other shells please see the manual configuration instructions below
and adapt those to the shell you use. Due to very restrictive security
policies of Mac OS X the following instructions differ slightly for
`MacOS` and `Linux`.

**Under Mac OS** please first double-click the folder containing the
data of the specific voice you would like to use as default voice to
open it in the Finder and then open a terminal window. Then back in the
window containing the ISiS_Vx.y.z folder locate the file
**Use_as_ISiS_default_voice.sh** and drag it onto the terminal window.
This will paste the complete path to the script into the terminal
command line Once you have copied the path to the terminal please click
on the terminal window and then simply hit `<return>`.

**Under Linux** please first double-click the folder containing the
voice data to open it in your file and directory browser. You can then
simply double-click the **Use_as_ISiS_default_voice.sh** script which
will execute the script.

The script will place an ISIS voice config file under your HOME
directory. For MacOSX the exact location of the file is

::

     ~/Library/Application\ Support/Ircam/ISiS_init_voice_rc.cfg

for Linux it is located under

::

      ~/.local/AppSupport/Ircam/ISiS_init_voice_rc.cfg

In the following you find the output that is generated for user vox
installing the RT voice in the `/Data/ISiS_DB` directory as the
default voice. The two lines are confirming u that you are using the
`/Data/ISiS_DB` directory as voice root and the `/Data/ISiS_DB/RT`
voice as your default voice.

::

   medcomp: (~) 5001> /Data/ISiS_DB/Use_as_ISiS_default_voice.sh
   ==========================================================================================
   ISiS configuration updated to use </Data/ISiS_DB> as root dir of ISiS voices
   and </Data/ISiS_DB/RT> as default voice.
   ==========================================================================================

Manual voice configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^

You can manually create or change the file that contains the ISiS voice
specifications.

For MacOSX the exact location of the file is

::

     ~/Library/Application\ Support/Ircam/ISiS_init_voice_rc.cfg

for Linux it is located under

::

      ~/.local/AppSupport/Ircam/ISiS_init_voice_rc.cfg

You can edit this file with arbirtrary text editors but need to save it
in text mode. Assuming that you placed your CORPORA under the
`/Data/ISiS_DB` directory and want to use the voice `RT` as your
default voice then you would need to create the file with the following
content

::

     [ISIS_VOICE_CONFIG]
     # root directory of ISiS voices
     ISIS_CORPORA: /Data/ISiS_DB
     # default voice sub directory
     ISIS_VOICE: /Data/ISiS_DB/RT

Setting voice defaults via environment variables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

YOu can temporarily overwrite the singing voice defaults using two
environment variables: the variable `ISIS_CORPORA` defines the root
directory containing all ISiS voices and the variable `ISIS_VOICE`
defines either the full path or the sub directory under `ISIS_CORPORA`
that contains the default voice.

If we assume as before that your voice root directory is
`/Data/ISiS_DB` and the voice directory containing the voice you want
to use as default is `/Data/ISiS_DB/RT`, and in case your shell is
`bash` then you can execute the following lines in our terminal to
switch the voice configuration without changing the config file.

.. code:: bash

   export ISIS_CORPORA='/Data/ISiS_DB'
   export ISIS_VOICE='RT'

You can set only the ISIS_VOICE variable if you want to change the
default voice without changing the voice root only. These changes will
are in effect for all subsequent runs of isis you perform in the
terminal where you issued these settings. To reset the change use

.. code:: bash

   unset ISIS_CORPORA
   unset ISIS_VOICE

If you don’t use bash please look into the documentation of your shell
to find how you can set envrionment variables.
