.. _software\_installation:

Installing the Software
~~~~~~~~~~~~~~~~~~~~~~~

ISiS is distributed via the `ISiS
page <http://forumnet.ircam.fr/product/isis/>`__ of the `IRCAM
Forum <http://forumnet.ircam.fr>`__ as a dmg or tar archive containing a
self contained command line application for `MacOS` (>=El Capitan) or
`Linux` (running on all platforms that are supported by `Anaconda
Python <https://www.anaconda.com/what-is-anaconda/>`__. While ISiS has
been developed in `Python` it comes as a binary executable so that you
don't need to install any dependencies besides the ISiS Software and the
singing voice packages. The software is currently in a beta stage, it
has been tested only on very few systems.

The installation is performed in the following **3 steps** that are
rather similar on `MacOS` and `Linux`. Please do not miss to test
your installation as described under
`testing <software_installation.html#testing-configuration>`__ after you
finished with step 3.

-  **Step 1) Unpacking**: The unpacking step is slightly different on
   `Linux` and `MacOS`. For **MacOS**: please locate and
   double-click the dmg ISiS\_Vx.y.z.dmg which will mount a disk image
   containing the application. For **Linux** there are many different
   file managers that behave slightly different and therefore we will
   not be able to describe how to extract the distribution from the
   tar.bz2 file. In most cases you should be able to locate the archive
   ISiS\_Vx.y.z.tar.bz2 in your file manager (For example Dolphin on KDE
   or Nautilus on gnome) and either double-click the file or right click
   the archive and select "extract here" to unpack it.

-  **Step 2) Relocating the software**: Move and rename the ISiS\_Vx.y.z
   directory you find in the mounted dmg (`MacOS`) or that you
   extracted from the tar (`Linux`) to a place where you like it to
   reside.

-  **Step 3) Configuring PATHs**: Finally you need to configure
   executable search path of your terminal so that the isis command will
   be found in the terminal. This can be performed automatically (using
   a shell script that comes with the ISiS distribution) or manually.
   The two procedures are described below.

   **Automatic PATH configuration**: The automatic shell script
   configuration currently supports the use of `bash` (the current
   default shell under `MacOS`) and `tcsh` shells. In case you use
   other shells please see the manual configuration instructions and
   adapt those to the shell you use. Due to very restrictive security
   policies of Mac OS X the following instructions differ slightly for
   `MacOS` and `Linux`.

   **Under MacOS** please first **double-click the ISiS\_Vx.y.z** folder
   to open it in the Finder and then open a terminal window. Then back
   in the Finder window containing the ISiS\_Vx.y.z folder grap the
   **Install\_ISiS\_commandline.sh** script and drag it with the mouse
   onto the terminal window. This will paste the complete path to the
   Install\_ISiS\_commandline.sh into the terminal command line Once you
   have copied the path to the terminal please click on the terminal
   window and then simply hit return.

   **Under Linux** please first **double-click the ISiS\_Vx.y.z** folder
   to open it in your file and directory browser. You can then simply
   double-click the **Install\_ISiS\_commandline.sh** script which will
   execute the script.

   The script will configure the terminal such that each time you open a
   new terminal the isis folder is added to your environment. In the
   following you find the output that is generated for user `vox`
   installing the ISiS software in his Applications directory on
   computer `medcomp` running `MacOS`, the output will be slightly
   different depending on your setup and OS, but the important line is
   the second last line telling you that you are set to run the ISiS
   application from your new installation.

   ::

       medcomp: (~) 501> /Users/vox/Applications/ISiS_V1.2.3/Install_ISiS_commandline.sh
       updated /Users/vox/Library/Application Support/Ircam/ISiS_init_rc.sh
       updated /Users/vox/Library/Application Support/Ircam/ISiS_init_rc.csh
       /Users/vox/.tcshrc is up to date.
       /Users/vox/.bashrc is up to date.
       /Users/vox/.bash_profile is up to date.
       ========================================================
       Shell configuration updated to use ISiS from /Users/vox/Applications/ISiS_V1.2.3
       ========================================================

   .. _manual-software-configuration:

   **Manual PATH configuration**: Depending on the shell you use you
   need to open the startup config file of the shell and add the
   directory ISiS software directory to your `PATH`. To find out which
   shell you use please open a terminal and type

   .. code:: bash

       echo $0

   and hit `<return>`. The shell will display the name of the shell
   evt with a leading dash.

   Hitting the `<return>` key anywhere in a line in the terminal
   indicates to the shell that you are finished typing and that the line
   should now be executed. To avoid extreme redundancy from now on, if
   you are asked to **execute** a command, then this will mean you
   should type the command and hit `<return>`.

   The shell configuration files are located in your `HOME` directory.
   The `HOME` directory is the directory where you are located when
   you are opening a new terminal. You can always go back to the
   `HOME` directory within your terminal by means of executing

   .. code:: bash

       cd 

   In the following directory names the `HOME` directory is indicated
   by means of the ~ sign. This sign is understood by all unix shells,
   so that you could also go back to your `HOME` directory by means of
   executing

   .. code:: bash

       cd ~

   For editing the files you can for example use the `nano` editor
   that is available on Linux and MacOS. Please note that while you may
   use TextEdit on MacOS the file open dialogs will in general not show
   the shell configuration files.

   Assuming again your username is `vox`, and you use `bash` as your
   shell, and the ISiS version is 1.2.3. In this case the software
   folder will be named ISiS\_V1.2.3 and if you want to install
   ISiS\_V1.2.3 into your local Applications directory the path to the
   software (on MacOS) will be `/Users/vox/Applications/ISiS_V1.2.3`.
   Accordingly you will need to add the following line to the file
   `~/.bash_profile`. For this you would execute

   .. code:: bash

       nano ~/.bash_profile

   scroll to the bottom, and on the beginning of a new line type

   .. code:: bash

       export PATH=/Users/vox/Applications/ISiS_V1.2.3:"$PATH"

   | The export command needs to be located on an individual line. To
     achieve this you hit `<return>` at the end of the line. Then you
     save the file by means of typing 'control'+'x' and selecting 'Y'
     when asked to confirm saving the changes. If you have problems
     operating `nano` you may read the description on changing
     `PATH` variable under `MacOS`
     `here <https://coolestguidesontheplanet.com/add-shell-path-osx/>`__.
   | Please note that you may not have a file `~/.bash_profile`, in
     which case the `nano` command mentioned above will automatically
     create it.

   In the case that you use `tcsh` or `csh` as your shell you would
   need to edit the file `~/.login` by means of executing

   .. code:: csh

       nano ~/.login

   and add the line at the end

   .. code:: csh

       set path = ( /Users/vox/Applications/ISiS_V1.2.3 $path )

   again - if you use `tcsh` or `csh` and this file does not exist
   `nano` will automatically create it.

   For other shells please refer to the documentation of the respective
   shell to see the names of the config files and the commands to be
   used to extend the `PATH`.

.. _software-config-important-notes:

Important Notes
^^^^^^^^^^^^^^^

The `PATH` configuration is permanent and you don't need to repeat it,
besides if you install a new version of ISiS or if you move the
ISiS\_Vx.y.z folder to a different location.

| The terminal configuration will be read whenever you open a **new
  terminal**, so
| after step 3 you need to open a new terminal to work with the ISiS
  command.

Under `MacOS` the `PATH` configuration described above will be
active only for programs you run from the terminal. Programs started via
the Finder - as for example Max/MSP - do not read these configuration
files. In case you would like to run ISiS from such a program, you need
to add the PATH configurations to the runtime environment of the
respective software. Please see the documentation of the software to
understand how to do this.

In case you would later like to relocate the software package, you can
repeat steps 2 and 3 whenever you want. In that case, however, only
terminals that are opened after the PATH have been configured in Step 3
will contain the new location of the software.

.. _testing-configuration:

Testing your configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^

Before working with the ISiS synthesis please test whether you have
successfully configured your environment. For this please open a new
terminal window and execute

.. code:: bash

    echo $PATH

You should see the ISiS\_Vx.y.z folder appearing somewhere at the start
of the string that is displayed in the terminal. In case you don't see
the folder, then please check whether you have correctly carried out the
configuration steps. Notably, after manual configuration please check
that the corresponding shell configuration files do exist and contain
the desired lines.
