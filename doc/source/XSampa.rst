.. _isis-xsampa-examples:

XSAMPA Examples
---------------

The following list provides examples for each XSAMPA symbol that is
supported in *ISiS*:

Vowels
~~~~~~

+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| XSAMPA Symbol   | API   | french word   | list of words                                                                                                       | explanation                                                                                                                                                                    |
+=================+=======+===============+=====================================================================================================================+================================================================================================================================================================================+
| a               | a     | la            | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/a/>`__              | `further reading <https://fr.wikipedia.org/wiki/Voyelle_ouverte_ant%C3%A9rieure_non_arrondie>`__                                                                               |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| e               | e     | beauté        | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/e/>`__              | `further reading <https://fr.wikipedia.org/wiki/Voyelle_mi-ferm%C3%A9e_ant%C3%A9rieure_non_arrondie>`__                                                                        |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| E               | ɛ     | cèpe          | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C9%9B/>`__         | `further reading <https://fr.wikipedia.org/wiki/Voyelle_mi-ouverte_ant%C3%A9rieure_non_arrondie>`__                                                                            |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 2               | ø     | peu           | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C3%B8/>`__         | `further reading <https://fr.wikipedia.org/wiki/Voyelle_mi-ferm%C3%A9e_ant%C3%A9rieure_arrondie>`__                                                                            |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 9               | œ     | jeune         | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C5%93/>`__         | `further reading <https://fr.wikipedia.org/wiki/Voyelle_mi-ouverte_ant%C3%A9rieure_arrondie>`__                                                                                |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| @               | ə     | menu          | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C9%99/>`__         | `further reading <https://fr.wikipedia.org/wiki/Voyelle_moyenne_centrale>`__                                                                                                   |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| i               | i     | vie           | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/i/>`__              | `further reading <https://fr.wikipedia.org/wiki/Voyelle_ferm%C3%A9e_ant%C3%A9rieure_non_arrondie>`__                                                                           |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| o               | o     | réseau        | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/o/>`__              | `further reading <https://fr.wikipedia.org/wiki/Voyelle_mi-ferm%C3%A9e_post%C3%A9rieure_arrondie>`__                                                                           |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| O               | ɔ     | sort          | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C9%94/>`__         | `further reading <https://fr.wikipedia.org/wiki/Voyelle_mi-ouverte_post%C3%A9rieure_arrondie>`__                                                                               |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| u               | u     | fou           | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/u/>`__              | `further reading <https://fr.wikipedia.org/wiki/Voyelle_ferm%C3%A9e_post%C3%A9rieure_arrondie>`__                                                                              |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| y               | y     | chute         | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/y/>`__              | `further reading <https://fr.wikipedia.org/wiki/Voyelle_ferm%C3%A9e_ant%C3%A9rieure_arrondie>`__                                                                               |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| o~              | ɔ~    | on            | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C9%94%CC%83/>`__   | `further reading <https://fr.wikipedia.org/wiki/Voyelle_moyenne_inf%C3%A9rieure_post%C3%A9rieure_arrondie>`__- `nasalised <https://fr.wikipedia.org/wiki/Nasalisation>`__      |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| a~              | ã     | banc          | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C9%91%CC%83/>`__   | `further reading <https://fr.wikipedia.org/wiki/Voyelle_basse_post%C3%A9rieure_non_arrondie>`__- `nasalised <https://fr.wikipedia.org/wiki/Nasalisation>`__                    |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| e~              | ɛ~    | bain          | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C9%9B%CC%83/>`__   | `further reading <https://fr.wikipedia.org/wiki/Voyelle_moyenne_inf%C3%A9rieure_ant%C3%A9rieure_non_arrondie>`__- `nasalised <https://fr.wikipedia.org/wiki/Nasalisation>`__   |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 9~              | œ~    | un            | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C5%93%CC%83/>`__   | `further reading <https://fr.wikipedia.org/wiki/Voyelle_moyenne_inf%C3%A9rieure_ant%C3%A9rieure_arrondie>`__ - `nasalised <https://fr.wikipedia.org/wiki/Nasalisation>`__      |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Semi-Vowels
~~~~~~~~~~~

+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------+
| XSAMPA Symbol   | API   | french word   | list of words                                                                                                 | explanation                                                                                            |
+=================+=======+===============+===============================================================================================================+========================================================================================================+
| w               | w     | aquarelle     | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/w/>`__        | `further reading <https://fr.wikipedia.org/wiki/Consonne_spirante_labio-v%C3%A9laire_vois%C3%A9e>`__   |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------+
| j               | j     | bille         | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/j/>`__        | `further reading <https://fr.wikipedia.org/wiki/Consonne_spirante_palatale_vois%C3%A9e>`__             |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------+
| H               | ɥ     | huit          | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C9%A5/>`__   | `further reading <https://fr.wikipedia.org/wiki/Consonne_spirante_labio-palatale_vois%C3%A9e>`__       |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------+

Fricatives
~~~~~~~~~~

+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+
| XSAMPA Symbol   | API   | french word   | list of words                                                                                                 | explanation                                                                                               |
+=================+=======+===============+===============================================================================================================+===========================================================================================================+
| v               | v     | avion         | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/v/>`__        | `further reading <https://fr.wikipedia.org/wiki/Consonne_fricative_labio-dentale_vois%C3%A9e>`__          |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+
| z               | z     | ros           | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/z/>`__        | `further reading <https://fr.wikipedia.org/wiki/Consonne_fricative_alv%C3%A9olaire_vois%C3%A9e>`__        |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+
| Z               | ʒ     | je            | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%CA%92/>`__   | `further reading <https://fr.wikipedia.org/wiki/Consonne_fricative_post-alv%C3%A9olaire_vois%C3%A9e>`__   |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+
| f               | f     | feu           | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/f/>`__        | `further reading <https://fr.wikipedia.org/wiki/Consonne_fricative_labio-dentale_sourde>`__               |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+
| s               | s     | brossent      | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/s/>`__        | `further reading <https://fr.wikipedia.org/wiki/Consonne_fricative_alv%C3%A9olaire_sourde>`__             |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+
| S               | ʃ     | chat          | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%CA%83/>`__   | `further reading <https://fr.wikipedia.org/wiki/Consonne_fricative_post-alv%C3%A9olaire_sourde>`__        |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+

Plosives
~~~~~~~~

+-----------------+-------+---------------+----------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------+
| XSAMPA Symbol   | API   | french word   | list of words                                                                                            | explanation                                                                                          |
+=================+=======+===============+==========================================================================================================+======================================================================================================+
| b               | b     | table         | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/b/>`__   | `further reading <https://fr.wikipedia.org/wiki/Consonne_occlusive_bilabiale_vois%C3%A9e>`__         |
+-----------------+-------+---------------+----------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------+
| d               | d     | demain        | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/d/>`__   | `further reading <https://fr.wikipedia.org/wiki/Consonne_occlusive_alv%C3%A9olaire_vois%C3%A9e>`__   |
+-----------------+-------+---------------+----------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------+
| g               | g     | bagues        | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/g/>`__   | `further reading <https://fr.wikipedia.org/wiki/Consonne_occlusive_v%C3%A9laire_vois%C3%A9e>`__      |
+-----------------+-------+---------------+----------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------+
| p               | p     | pot           | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/p/>`__   | `further reading <https://fr.wikipedia.org/wiki/Consonne_occlusive_bilabiale_sourde>`__              |
+-----------------+-------+---------------+----------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------+
| t               | t     | compter       | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/t/>`__   | `further reading <https://fr.wikipedia.org/wiki/Consonne_occlusive_alv%C3%A9olaire_sourde>`__        |
+-----------------+-------+---------------+----------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------+
| k               | k     | lac           | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/k/>`__   | `further reading <https://fr.wikipedia.org/wiki/Consonne_occlusive_v%C3%A9laire_sourde>`__           |
+-----------------+-------+---------------+----------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------+

Nasals
~~~~~~

+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------+
| XSAMPA Symbol   | API   | french word   | list of words                                                                                                 | explanation                                                                                                 |
+=================+=======+===============+===============================================================================================================+=============================================================================================================+
| m               | m     | maison        | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/m/>`__        | `further reading <https://fr.wikipedia.org/wiki/Consonne_nasale_bilabiale_vois%C3%A9e>`__                   |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------+
| n               | n     | nez           | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/n/>`__        | `further reading <https://fr.wikipedia.org/wiki/Consonne_occlusive_nasale_alv%C3%A9olaire_vois%C3%A9e>`__   |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------+
| N               | ŋ     | camping       | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C5%8B/>`__   | `further reading <https://fr.wikipedia.org/wiki/Consonne_occlusive_nasale_v%C3%A9laire_vois%C3%A9e>`__      |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------+

Others
~~~~~~

+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------------+
| XSAMPA Symbol   | API   | french word   | list of words                                                                                                 | explanation                                                                                                       |
+=================+=======+===============+===============================================================================================================+===================================================================================================================+
| R               | ʁ     | tarte         | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%CA%81/>`__   | `further reading <https://fr.wikipedia.org/wiki/Consonne_fricative_uvulaire_vois%C3%A9e>`__                       |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------------+
| l               | l     | lait          | `examples <https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/l/>`__        | `further reading <https://fr.wikipedia.org/wiki/Consonne_spirante_lat%C3%A9rale_alv%C3%A9olaire_vois%C3%A9e>`__   |
+-----------------+-------+---------------+---------------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------------+

Online resources
----------------

To help you to convert a given french text you can make use of online
resources. because online resources directly using XSAMPA are hard to
find generally you wil have to pass via the `international phonetic
alphabet
(IPA) <https://en.wikipedia.org/wiki/International_Phonetic_Alphabet>`__.

Text to IPA
~~~~~~~~~~~

To get the IPA transcription of a given french word you can either use
online dictionaries as for example `www.
wordreference.com <https://www.wordreference.com/fren/>`__, or even
transcribe a given phrase completely into IPA using `French to
IPA <https://easypronunciation.com/en/french-phonetic-transcription-converter#phonetic_transcription>`__.

IPA to XSAMPA
~~~~~~~~~~~~~

In a second step you can then use the `IPA to SAMPA
converter <http://www.lfsag.unito.it/ipa/converter_en.html>`__ to get
the transcription in the SAMPA alphabet, which for the part of the IPA
that is covered by *ISiS* corresponds more or less with the XSAMPA
implemented in *ISiS*. The only difference are the nasalised vowels that
in *ISiS* are always lower-case, while in the SAMPA transcription
produced by means of `IPA to SAMPA
converter <http://www.lfsag.unito.it/ipa/converter_en.html>`__ there is
a difference between e~ ad E~. So when constructing the *ISiS* score you
should convert all nasalised vowels, that are letters with a "~"
attached, from upper-case into the corresponding lower-case letter.
